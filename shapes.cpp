#include "shapes.h"
#include <cmath>

Shape::Shape(std::string name)
{
  this->name = name;
}

//destructor fdojfdsuhfdifhsdiuh
Shape::~Shape()
{
}

std::string Shape::description()
{
  return name + " has area: " + std::to_string(area());
}

Square::Square(double width) : Shape("Square")
{
  this->width = width;
}

Rectangle::Rectangle(double width, double height) : Shape("Rectangle")
{
  this->width = width;
  this->height = height;
}

Circle::Circle(double radius) : Shape("Circle")
{
  this->radius = radius;
}

double Square::area()
{
  return width * width;
}

double Rectangle::area()
{
  return width * height;
}

double Circle::area()
{
  return M_PI * radius * radius;
}