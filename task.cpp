#include "shapes.h"
#include <vector>
#include <iostream>


int main(){

  //check if everything works
  Square sq1(5);
  std::cout << "First test shape description and area is: " << sq1.description() << "\n";

  //creating vector for the shapes
  std::vector <Shape*> my_shapes;
  my_shapes.push_back(new Square(3));
  my_shapes.push_back(new Circle(2));
  my_shapes.push_back(new Rectangle(2,3));

  //creating loop for printing vector
  for (size_t i = 0; i < my_shapes.size(); i++ )
  {
    //printing adresses (pointer)
    std::cout << my_shapes[i] << "  \n"; 
    //and dereference
    std::cout << my_shapes[i]->description() << "  \n"; 
  }


  return 0;
}